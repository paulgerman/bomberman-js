import express from "express";
import {getRandomInt, newID} from "./utils";
import * as fs from "fs";
import Player from "../../client/src/scripts/engine/Player";
import {Powerup, PowerupType} from "../../client/src/scripts/engine/Powerup";
import map1 from "./assets/maps/map1";
import cors from "cors";
import ObjectPosition from "../../client/src/scripts/engine/ObjectPosition";
import Enemy from "../../client/src/scripts/engine/Enemy";
import { NOTHING } from "../../client/src/scripts/engine/constants";
import { isValidMovePosition } from "../../client/src/scripts/utils";

const app = express();
app.use(cors());

const http = require("http").Server(app);

const io = require('socket.io')(http);
const port = 8080;

export default class GameServer{
    rooms = {};
    players = {};

    public run()
    {
        console.log("running");
        io.on('connection', async (socket) => {
            const userID = socket.handshake.query.id;
            const roomName = socket.handshake.query.room;

            socket.join(roomName);

            if(this.rooms[roomName] === undefined)
                await this.createRoom(roomName)

            this.rooms[roomName].users.push(userID);

            io.to(roomName).emit('playersList', this.rooms[roomName]);

            console.log(roomName + ":" + userID + " connected!");

            socket.on('die', (msg) => {
                console.log(roomName + ": died", msg);
                socket.broadcast.to(roomName).emit('die', msg);
            });

            socket.on('bomb', (msg) => {
                console.log(roomName + ": bomb", msg);
                socket.broadcast.to(roomName).emit('bomb', msg);
            });

            socket.on('powerupDestroy', (powerupId) => {
                console.log(roomName + ": powerupDestroy", powerupId);
                socket.broadcast.to(roomName).emit('powerupDestroy', powerupId);
                delete this.rooms[roomName].powerups[powerupId];
            });

            socket.on('playerUpdate', (player: Player) => {
                console.log(roomName + ":", player);
                this.players[player.id] = player;
                socket.broadcast.to(roomName).emit('playerUpdate', player);
            });

            socket.on('refresh', (msg) => {
                console.log(roomName + ":" + "refresh");
                socket.broadcast.to(roomName).emit('refresh', msg);
            });

            socket.on('disconnect', () => {
                console.log(roomName + ":" + userID + ' disconnected');
                const index = this.rooms[roomName].users.indexOf(userID);
                this.rooms[roomName].users.splice(index, 1);
                delete this.players[userID];

                io.to(roomName).emit('disconnect', userID);
            });
        });

        http.listen(port, function () {
            console.log('listening on *:' + port);
        });
    }

    private async createRoom(roomName: string) {
        const map = this.loadMap();
        const enemies: Enemy[] = [];

        this.rooms[roomName] = {
            config: {}, 
            users: [],
            map,
            powerups: {},
            enemies,
        };
        setInterval((roomName) => { this.addPowerup(roomName); }, 7000, roomName);
        setInterval((roomName) => { this.moveEnemies(roomName) }, 500, roomName);

        console.log("miau");
        for (let i = 0; i < 2; i++) {
            const startPos: ObjectPosition = this.getStartPosition(roomName, map);
            const enemy: Enemy = {
                id: newID(),
                pos: startPos,
                lastMoveX: 0,
                lastMoveY: 0,
            }
            enemies.push(enemy);
        }
    }

    private moveEnemies(roomName) {
        this.rooms[roomName].enemies.forEach((enemy: Enemy) => {
            console.log(enemy)
            if (!(enemy.lastMoveX == 0 && enemy.lastMoveY == 0) && isValidMovePosition(this.rooms[roomName].map, enemy.pos, enemy.lastMoveX, enemy.lastMoveY)) {
                enemy.pos.posX += enemy.lastMoveX;
                enemy.pos.posY += enemy.lastMoveY;
                //console.log("Same move!")
            } else {
                //console.log("diff move!" +  enemy.lastMoveX + " " + enemy.lastMoveY)
                const validMoves = [];
                for (let pos of [{x: 1, y: 0}, {x: 0, y: 1}, {x: -1, y: 0}, {x: 0, y: -1}])
                {
                    if (isValidMovePosition(this.rooms[roomName].map, enemy.pos, pos.x, pos.y))
                    {
                        validMoves.push(pos)
                    }
                }
                const move = validMoves[getRandomInt(0, validMoves.length - 1)];
                enemy.pos.posX += move.x;
                enemy.pos.posY += move.y;
                enemy.lastMoveX = move.x;
                enemy.lastMoveY = move.y;
            }

            io.to(roomName).emit('enemyUpdate', enemy);
        });
    }

    private addPowerup(roomName: string) {
        console.log("adding powerup");
        const powerupsNum = Object.keys(this.rooms[roomName].powerups).length;
        if(powerupsNum >= 5) {
            return;
        }

        const rand = getRandomInt(0, 2)

        let powerup: Powerup;
        const startPos = this.getStartPosition(roomName, this.rooms[roomName].map);
        if(rand === 0)
        {
            powerup = {
                id: newID(),
                duration: 10000,
                modifier: 10,
                type: PowerupType.EXTEND_EXPLODING_RANGE,
                posX: startPos.posX,
                posY: startPos.posY,
            }
        }
        else if(rand === 1)
        {
            powerup = {
                id: newID(),
                duration: 10000,
                modifier: 1000,
                type: PowerupType.EXTEND_EXPLOSION_TIME,
                posX: startPos.posX,
                posY: startPos.posY,
            }
        }
        else if(rand === 2)
        {
            powerup = {
                id: newID(),
                duration: -1,
                modifier: 1,
                type: PowerupType.EXTRA_LIFE,
                posX: startPos.posX,
                posY: startPos.posY,
            }
        }
        this.rooms[roomName].powerups[powerup.id] = powerup;
        io.to(roomName).emit('powerupSpawn', powerup);
    }

    loadMap()
    {
        const data = map1;
        return this.parseMap(data)
    }

    parseMap(mapRaw) {
        const rows = mapRaw.split('\n');

        let map = [];
        for (let i = 0; i < rows.length; i++) {
            const cols = rows[i].split(',').map(Number);

            map.push(cols);
        }
        map = this.transpose(map);
        this.debugMap(map);
        return map
    }

    transpose(matrix) {
        return matrix[0].map((col, i) => matrix.map(row => row[i]));
    }

    debugMap(map){
        const mapCopy = JSON.parse(JSON.stringify(map));

        console.table(this.transpose(mapCopy));
    }

    getStartPosition(roomName, map){
        let posX, posY

        do {
            posX = getRandomInt(0, map[0].length - 1)
            posY = getRandomInt(0, map[0].length - 1)
        }
        while (
            map[posX][posY] != 0 ||
            Object.values(this.rooms[roomName].powerups as Powerup[]).some(p => p.posX == posX && p.posY == posY)
        ) {}


        return {posX: posX, posY: posY};
    }
}
