export enum PowerupType {
    EXTEND_EXPLODING_RANGE,
    EXTEND_EXPLOSION_TIME,
    EXTRA_LIFE,
}

export interface Powerup{
    id: string,
    type: PowerupType,
    duration: number,
    modifier: number,
    posX: number,
    posY: number
}