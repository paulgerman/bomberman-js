import { BOMB_EXPLODE_SIZE, BOMB_EXPLODE_TIME, BOMB_EXPLOSION_TIME, BOMB_REPLENISH_TIME, NOTHING } from './constants'
import Player from './Player'
import io from 'socket.io-client';
import UI from '../ui/UI';
import Bomb from './Bomb'
import { getBombExplodeCells, getRandomInt, newID } from '../utils'
import {Powerup} from "./Powerup";
import Enemy from './Enemy';

export default class GameEngine {
    player: Player
    players: { [key: string] : Player } = {}
    enemies: { [key: string] : Enemy } = {}
    bombs: { [key: string] : Bomb } = {}
    powerups: { [key: string] : Powerup } = {}
    map: integer[][] = [];
    private socket: io
    UI: UI
    private roomName: string
    remainingBombs: number = 2

    lastAddedBomb: number = 0

    bombResetInterval: number | null = null;

    init(){
        document.addEventListener('keydown', (event) => {
            this.onInput(event.key);
        }, false);
    }

    constructor(roomName, userId, userName, socket, map, powerups) {
        this.init();
        this.roomName = roomName;
        this.socket = socket;
        this.map = map;
        this.powerups = powerups;

        this.ready(userId, userName)
    }

    bombReplenish() {
        if (this.remainingBombs < 2) {
            this.remainingBombs += 1
        } else if (this.bombResetInterval) {
            clearInterval(this.bombResetInterval)
            this.bombResetInterval = null;
        }
    }

    async ready(userId, userName){
        await this.startUI()

        for(let id in this.powerups)
        {
            this.UI.powerupSpawn(this.powerups[id])
        }
        this.addEvents();

        const startPosition = this.getStartPosition();
        this.player = new Player(userId, userName, startPosition, this);
        
        this.addPlayer(this.player);

        this.player.emit();

        //get all current players when I first join
        this.requestRefresh();
    }

    async startUI(){
        this.UI = new UI(this)
        await this.UI.loaded.promise
    }

    addEvents(){
        this.socket.on('playerUpdate', (player) => {
            this.onUpdatePlayer(player);
            this.UI.updatePlayer(player);
        });

        //if someone requested refresh, send it
        this.socket.on('refresh', (_) => {
            this.player.emit();
        });

        //when someone puts a bomb
        this.socket.on('bomb', (bomb) => {
            this.addBomb(bomb)
        });

        this.socket.on('disconnect', (playerId: string) => {
            delete this.players[playerId];
            this.UI.disconnect(playerId);
        });

        this.socket.on('powerupSpawn', (powerup: Powerup) => {
            this.powerups[powerup.id] = powerup;
            this.UI.powerupSpawn(powerup);
        });

        this.socket.on('enemyUpdate', (enemy: Enemy) => {
            this.enemies[enemy.id] = enemy;
            this.UI.updateEnemy(enemy);
        });

        this.socket.on('powerupDestroy', (powerupId: string) => {
            delete this.powerups[powerupId];
            this.UI.powerupDestroy(powerupId);
        });

        //when someone dies
        //for now, only if I die, I respawn myself and emit my new position
        this.socket.on('die', (player) => {
            if(player.id === this.player.id){
                this.suicide();
            }

            //trigger die animation?
        });
    }


    onUpdatePlayer(player: Player) {
        console.log('player received', player);
        this.players[player.id] = player;

        if(player.id == this.player.id)
        {
            this.player.lifes = player.lifes
            this.player.score = player.score
        }
    }

    
    enemyUpdate(enemy: Enemy) {
        console.log('enemy received', enemy);
    }

    debugMap(){
        const mapCopy = JSON.parse(JSON.stringify(this.map));
        for(let id in this.players){
            mapCopy[this.players[id].pos.posX][this.players[id].pos.posY] = "U";
        }

        for(let id in this.bombs){
            mapCopy[this.bombs[id].posX][this.bombs[id].posY] = "B";
        }
        console.table(this.transpose(mapCopy));
    }

    onInput(key){
        console.log(key);
        switch (key) {
            case "ArrowLeft":
            case "A":
            case "a":
                this.player.move(-1, 0);
                break;
            case "ArrowRight":
            case "D":
            case "d":
                this.player.move(1, 0);
                break;
            case "ArrowUp":
            case "W":
            case "w":
                this.player.move(0, -1);
                break;
            case "ArrowDown":
            case "S":
            case "s":
                this.player.move(0, 1);
                break;
            case " ":
                const bomb = {
                    id: this.player.id + ":" + newID(),
                    posX: this.player.pos.posX,
                    posY: this.player.pos.posY,
                    owner: this.player.id
                };
                if (this.remainingBombs > 0) {
                    this.addBomb(bomb);
                    this.lastAddedBomb = Date.now();
                    this.remainingBombs -= 1;
                    if (!this.bombResetInterval)
                        this.bombResetInterval = window.setInterval(this.bombReplenish.bind(this), BOMB_REPLENISH_TIME)
                }
        }        
    }

    addBomb(bomb: Bomb){
        console.log("Added bomb ", bomb.id);
        this.bombs[bomb.id] = bomb;

        //only emit if it is my bomb
        if(bomb.owner === this.player.id)
            this.emit("bomb", bomb);

        setTimeout(() => {
            this.explodeBomb(bomb);
        }, BOMB_EXPLODE_TIME);

        this.UI.addBomb(bomb);
    }

    explodeBomb(bomb: Bomb){
        console.log("Bomb exploded ", bomb.id);

        //only act on the bombs I put
        if(bomb.owner === this.player.id) {
            const explodedTiles = getBombExplodeCells(bomb, this.map, this.players[bomb.owner].bombExplodingRange)
            bomb.explodedTiles = explodedTiles;

            for (let i in explodedTiles) {
                const newX = explodedTiles[i].posX;
                const newY = explodedTiles[i].posY;

                for (let i in this.players) {
                    if (this.players[i].pos.posX === newX && this.players[i].pos.posY === newY) {
                        this.hitPlayer(this.players[i]);
                        console.log("Player ", this.players[i], " killed by bomb ", bomb);
                    }
                }
            }
        }
        setTimeout(() => {
            if(this.bombs[bomb.id]) {
                delete this.bombs[bomb.id];
                console.log("DELETED BOMB " + bomb.id);
            }
            else
                console.error("BOMB " + bomb.id + " DOES NOT EXIST!");
        }, this.players[bomb.owner].bombExplosionTime)
    }

    hitPlayer(player: Player){
        player.lifes--;
        if(player.lifes == 0)
        {
            if(player.id === this.player.id) {
                this.player.score--;
                this.suicide();
            } else {
                this.emit("die", player);
                this.player.score++;
                this.player.emit();
                console.log(`${player.name} killed by me. ${this.player.name} new score: ${this.player.score}`);
            }
        }
        else
            this.emitPlayer(player);
    }

    suicide(){
        console.log("suiciding");
        const startPosition = this.getStartPosition();
        this.player.pos = startPosition;
        this.player.lifes = 1;
        this.UI.updatePlayer(this.player)
        this.player.emit();
    }

    emit(type, msg){
        this.socket.emit(type, msg);
        console.log(type, msg);
    }

    //request all players to send their info to me
    requestRefresh(){
        this.socket.emit('refresh', '');
        console.log("refreshed!");
    }

    addPlayer(player: Player){
        this.players[player.id] = player;
        this.UI.updatePlayer(player);
    }

    //randomly select one of the corners
    getStartPosition(){
        let posX, posY
        do {
            posX = getRandomInt(0, this.map[0].length - 1)
            posY = getRandomInt(0, this.map[0].length - 1)
        }
        while (
            this.map[posX][posY] != 0 ||
            Object.values(this.players).some(p => p.pos.posX == posX && p.pos.posY == posY)
        )

        console.log("New positions: " + posX + "|" + posY);
        return {posX: posX, posY: posY};
    }

    //generate 0 or 1 randomly
    randBit(){
        let x =Math.random();
        if(x < 0.5)
            x = Math.floor(x);
        else
            x = Math.ceil(x);

        return x;
    }

    toJSON(){
        return "";
    }

    transpose(matrix) {
        return matrix[0].map((col, i) => matrix.map(row => row[i]));
    }

    emitPlayer(player: Player){
        this.emit('playerUpdate', player);
    }
}