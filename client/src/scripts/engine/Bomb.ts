import ObjectPosition from './ObjectPosition'

export default interface Bomb{
  id: string,
  posX: integer,
  posY: integer,
  owner: string,
  explodedTiles?: ObjectPosition[]
}