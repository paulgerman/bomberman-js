import ObjectPosition from "./ObjectPosition";

export default interface Enemy{
    id: string,
    pos: ObjectPosition,
    lastMoveX: number,
    lastMoveY: number,
}