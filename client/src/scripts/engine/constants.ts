export const NOTHING:integer = 0;
export const WALL:integer = 1;

export const S:integer = 1000;
export const BOMB_EXPLODE_TIME: integer = 1.2 * S;
export const BOMB_EXPLOSION_TIME: integer = 1 * S;
export const BOMB_EXPLODE_SIZE: integer = 4;

export const BOMB_REPLENISH_TIME: integer = 1000;

export const PLAYER_MOVE_DELAY = 200;


export const DEFAULT_WIDTH = 900
export const DEFAULT_HEIGHT = 600

export const SERVER = "http://localhost:8080"

export const CHARACTERS_NUM = 56