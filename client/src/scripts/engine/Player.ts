import GameEngine from './GameEngine'
import { BOMB_EXPLODE_SIZE, BOMB_EXPLOSION_TIME, CHARACTERS_NUM, PLAYER_MOVE_DELAY, NOTHING } from './constants'
import { getRandomInt, isValidMovePosition } from '../utils'
import { Powerup, PowerupType } from "./Powerup";
import ObjectPosition from './ObjectPosition';

export default class Player{
    static charactersTaken: integer[] = []

    id: string
    name: string
    pos: ObjectPosition
    character: integer
    score: integer = 0
    lastMoved: number = 0

    private game: GameEngine
    private moveTimeout: number | null = null

    bombExplodingRange: number = BOMB_EXPLODE_SIZE
    bombExplosionTime: number = BOMB_EXPLOSION_TIME
    lifes: number = 1

    constructor(id: string, name: string, pos: ObjectPosition, game: GameEngine) {
        this.id = id;
        this.name = name;
        this.pos = pos;
        this.game = game;

        let character: integer;
        do {
            character = getRandomInt(0, CHARACTERS_NUM)
        } while(Player.charactersTaken.includes(character) && character == 51)
        this.character = character
    }

    move(x, y){
        const elapsed = Date.now() - this.lastMoved;
        if (!(elapsed >= PLAYER_MOVE_DELAY)) {
            console.log("Can't move yet")
            if (this.moveTimeout != null) {
                window.clearTimeout(this.moveTimeout)
            }
            this.moveTimeout = window.setTimeout((x, y) => this.move(x, y), PLAYER_MOVE_DELAY - elapsed, x, y)
        } else {
            if (isValidMovePosition(this.game.map, this.pos, x, y)) {
                for(let id in this.game.players)
                {
                    if(this.game.players[id].pos.posX === this.pos.posX + x && this.game.players[id].pos.posY === this.pos.posY + y) {
                        console.log("invalid move! crossing another player!");
                        return;
                    }
                }
                for(let id in this.game.bombs)
                {
                    const bomb = this.game.bombs[id]
                    if(bomb.posX === this.pos.posX + x && bomb.posY === this.pos.posY + y) {
                        console.log("invalid move! crossing a bomb!");
                        return;
                    }
                    if(bomb.explodedTiles)
                        for(let i in bomb.explodedTiles)
                            if(bomb.explodedTiles[i].posX == this.pos.posX + x && bomb.explodedTiles[i].posY == this.pos.posY + y) {
                                console.log("Collided with bomb explosion!");
                                this.score--;
                                this.game.suicide()
                                return;
                            }
                }
                this.pos.posX += x;
                this.pos.posY += y;

                for(let i in this.game.powerups)
                {
                    const powerup = this.game.powerups[i]
                    if (powerup.posX == this.pos.posX && powerup.posY == this.pos.posY)
                    {
                        this.game.emit("powerupDestroy", powerup.id);
                        this.activatePowerup(powerup)
                    }
                }

                this.game.UI.updatePlayer(this);
                this.emit();
                this.lastMoved = Date.now();
            } else {
                console.log("invalid move!");
            }
        }
    }

    emit(){
        console.log(JSON.stringify(this));
        this.game.emit('playerUpdate', this);
    }

    activatePowerup(powerup: Powerup) {
        if(powerup.type === PowerupType.EXTEND_EXPLODING_RANGE) {
            this.bombExplodingRange += powerup.modifier
            console.log("Powerup extend explode range by " + powerup.modifier + " activated for " + powerup.duration + "ms");
            setTimeout(() => {
                console.log("Powerup extend explode range DEACTIVATED");
                this.bombExplodingRange -= powerup.modifier
                this.emit()
            }, powerup.duration)
        }
        else if(powerup.type === PowerupType.EXTEND_EXPLOSION_TIME) {
            this.bombExplosionTime += powerup.modifier
            console.log("Powerup extend explosion time by " + powerup.modifier + " activated for " + powerup.duration + "ms");
            setTimeout(() => {
                console.log("Powerup extend explosion time DEACTIVATED");
                this.bombExplosionTime -= powerup.modifier
                this.emit()
            }, powerup.duration)
        }
        else if(powerup.type === PowerupType.EXTRA_LIFE) {
            console.log("Powerup extra life by " + powerup.modifier + " activated");
            this.lifes += powerup.modifier
        }

        delete this.game.powerups[powerup.id];
        this.game.UI.powerupDestroy(powerup.id);
        this.emit()
    }
}