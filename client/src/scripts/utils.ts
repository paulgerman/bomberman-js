import Bomb from './engine/Bomb'
import ObjectPosition from './engine/ObjectPosition'
import { BOMB_EXPLODE_SIZE, NOTHING } from './engine/constants'

export function getRandomInt(min, max): integer {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getBombExplodeCells(bomb: Bomb, map: integer[][], bombExplodeSize): ObjectPosition[] {
  const directions = [[0,1],[0,-1],[1,0],[-1,0]];

  const coordinatesToCheck = [{posX: bomb.posX, posY: bomb.posY} as ObjectPosition];
  for (let i in directions) //x axis left and right
    for (let k = 1; k <= bombExplodeSize; k++) {
      const newX = bomb.posX + directions[i][0] * k;
      const newY = bomb.posY + directions[i][1] * k;

      if (!(
        newX >= 0 && newX < map.length
        && newY >= 0 && newY < map[0].length
        && map[newX][newY] === NOTHING
      ))
        break;

      coordinatesToCheck.push({
        posX: newX,
        posY: newY,
      } as ObjectPosition);
    }
  return coordinatesToCheck
}

export function isValidMovePosition(map: integer[][], startingPos: ObjectPosition, x: number, y: number): boolean {
  return startingPos.posX + x >= 0 && startingPos.posX + x < map.length
      && startingPos.posY + y >= 0 && startingPos.posY + y < map[0].length
      && map[startingPos.posX + x][startingPos.posY + y] === NOTHING;
}

export function newID(): string {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}