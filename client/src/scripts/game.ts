import UI from './ui/UI'
import GameEngine from "./engine/GameEngine"
import io from 'socket.io-client';
import { SERVER } from './engine/constants';
import Random = Phaser.Math.Angle.Random

window.addEventListener('load', () => {
  let map;
  let powerups;

  document.getElementById("config")!.addEventListener("click", () => {
    // @ts-ignore
    const userId = document.getElementById("name")!.value;

    const userName = userId;
    // @ts-ignore
    const roomName = document.querySelector('input[name="room"]:checked')!.value;

    const socket = io(SERVER, {query:'id=' + userId + "&room=" + roomName});


    socket.on('playersList', (room) => {
      document.getElementById("configForm")!.style.display = "none";
      document.getElementById("startForm")!.style.display = "block";
      document.getElementById("list")!.innerHTML = room.users;
      map = room.map;
      powerups = room.powerups;

      console.log("playersList received", room.users);

      // @ts-ignore
      document.getElementById("start")!.disabled = room.users.length < 2;
    });

    document.getElementById("start")!.addEventListener("click", () => {
      // @ts-ignore
      if(document.getElementById("start")!.disabled)
        return;

      // @ts-ignore
      window.game = new GameEngine(roomName, userId, userName, socket, map, powerups);
      document.getElementById("startForm")!.style.display = "none";
      document.getElementById("game")!.style.display = "block";
    });


    console.log("added");

  });

//  const userId = "p" + Random().toString().slice(-5);
//   const socket = io(SERVER, {query:'id=' + userId + "&room=p"});
//   socket.on('playersList', (room) => {
//     map = room.map;
//     powerups = room.powerups;
//   });
//   setTimeout(() => {
//     // @ts-ignore
//     window.game = new GameEngine("p", userId, userId, socket, map, powerups);
//     document.getElementById("configForm")!.style.display = "none";
//     document.getElementById("startForm")!.style.display = "none";
//     document.getElementById("game")!.style.display = "block";
//   }, 1000);
})

