import 'phaser'
import { DEFAULT_HEIGHT, DEFAULT_WIDTH } from '../engine/constants'
import MainScene from './scenes/MainScene'
import PreloadScene from './scenes/PreloadScene'
import { Game } from 'phaser'
import Bomb from '../engine/Bomb'
import preloadScene from './scenes/PreloadScene'
import Player from '../engine/Player'
import {Deferred} from "ts-deferred";
import {Powerup} from "../engine/Powerup";
import GameEngine from "../engine/GameEngine";
import Enemy from '../engine/Enemy'

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  backgroundColor: '#ffffff',
  scale: {
    parent: 'phaser-game',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT
  },
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
      gravity: { y: 400 }
    }
  }
}

export default class UI extends Game{
  mainScene: MainScene
  preloadScene: PreloadScene
  loaded: Deferred<boolean> = new Deferred<boolean>()

  constructor(game: GameEngine) {
    super(config)

    this.events.on('ready', () => {
      this.preloadScene = <PreloadScene> this.scene.add("PreloadScene", PreloadScene, true)
      this.mainScene = <MainScene> this.scene.add("MainScene", MainScene, false, game)

      this.preloadScene.load.once('complete', () =>
      {
        this.loaded.resolve(true)
      });
    })
  }

  addBomb(bomb: Bomb) {
    this.mainScene.addBomb(bomb);
  }

  updatePlayer(player: Player) {
    this.mainScene.updatePlayer(player);
  }

  updateEnemy(enemy: Enemy) {
    this.mainScene.updateEnemy(enemy);
  }

  disconnect(playerId: string) {
    this.mainScene.disconnect(playerId);
  }

  powerupSpawn(powerup: Powerup) {
    this.mainScene.powerupSpawn(powerup);
  }

  powerupDestroy(powerupId: string) {
    this.mainScene.powerupDestroy(powerupId);
  }
}

