import PhaserLogo from '../objects/phaserLogo'
import FpsText from '../objects/fpsText'
import Player from '../../engine/Player'
import Bomb from '../../engine/Bomb'
import Image = Phaser.GameObjects.Image
import { BOMB_EXPLODE_TIME } from '../../engine/constants'
import { getBombExplodeCells } from '../../utils'
import Sprite = Phaser.GameObjects.Sprite
import { Powerup, PowerupType } from "../../engine/Powerup";
import GameEngine from "../../engine/GameEngine";
import { GameObjects } from 'phaser'
import Enemy from '../../engine/Enemy'
import ObjectPosition from '../../engine/ObjectPosition'

export default class MainScene extends Phaser.Scene {


  playerImages: { [key: string] : Image } = {}
  enemyImages: { [key: string] : Image } = {}
  bombImages: { [key: string] : Image } = {}
  powerupImages: { [key: string] : Image } = {}

  explodeAnimation: Phaser.Animations.Animation
  gameEngine: GameEngine

  scoreContainer: Phaser.GameObjects.Container
  livesText: Phaser.GameObjects.Text
  scoreLines: { sprite: Phaser.GameObjects.Sprite, text: Phaser.GameObjects.Text }[] = []
  bomb1: Phaser.GameObjects.Image
  bomb2: Phaser.GameObjects.Image

  init(gameEngine: GameEngine) {
    this.gameEngine = gameEngine;
  }

  preload(){
    const explodeAnimation = this.anims.create({
      key: 'explode',
      frames: this.anims.generateFrameNumbers('explosion', { start: 0, end: 46 }),
      frameRate: 46,
      repeat: -1
    });
    if(explodeAnimation === false)
      throw new Error("Animation not loaded!");

    this.explodeAnimation = explodeAnimation;
  }

  constructor() {
    super({ key: 'MainScene' })
  }

  create() {
    const map = this.make.tilemap({ key: 'map', tileWidth: 32, tileHeight: 32 })
    console.log(map);
    const tileset = map.addTilesetImage('tiles', undefined, 32, 32, 1, 2)
    const layer = map.createStaticLayer(0, tileset, 0, 0)


    this.scoreContainer = this.add.container(800, 5);
    this.livesText = this.add.text(0, 0, '', { font: '14px Courier', fill: '#000000' });
    this.scoreContainer.add(this.livesText)

    this.bomb1 = this.add.image(25, 550, 'bomb')
    this.bomb1.setDisplaySize(32, 32)

    this.bomb2 = this.add.image(65, 550, 'bomb')
    this.bomb2.setDisplaySize(32, 32)

    this.scoreContainer.add([this.livesText, this.bomb1, this.bomb2])


    //update UI
    setInterval(() => {
      this.livesText.setText(this.getLivesText())
      this.updateScore();
      this.updateBombs(this.gameEngine.remainingBombs);
    }, 50);

  }

  update() {
    //this.fpsText.update()
  }

  addBomb(bomb: Bomb) {
    if (!this.gameEngine)
      return;

    this.bombImages[bomb.id] = this.add.image(32 * bomb.posX + 16, 32 * bomb.posY + 16, 'bomb')
    this.bombImages[bomb.id].setDisplaySize(32, 32)
    const bombExplodingRange = this.gameEngine.players[bomb.owner].bombExplodingRange;
    const bombExplosionTime = this.gameEngine.players[bomb.owner].bombExplosionTime;

    console.log("range " + bombExplodingRange + " time: " + bombExplosionTime);

    setTimeout((bomb: Bomb, bombExplodingRange, bombExplosionTime) => {
      this.bombImages[bomb.id]?.destroy()
      delete this.bombImages[bomb.id]

      const cellsToExplode = getBombExplodeCells(bomb, this.gameEngine.map, bombExplodingRange)
      const sprites: Sprite[] = []
      for(let i in cellsToExplode) {
        const sprite = this.add.sprite(32 * cellsToExplode[i].posX + 16, 32 * cellsToExplode[i].posY + 16, 'explosion');
        sprite.play("explode");
        sprites.push(sprite)
      }
      setTimeout(() => {
        sprites.map((sprite) => {sprite.destroy()})
      }, bombExplosionTime)

    }, BOMB_EXPLODE_TIME, bomb, bombExplodingRange, bombExplosionTime)
  }

  updateBombs(bombNum: number) {
    if (bombNum == 0) {
      this.bomb2.alpha = 0.4
      this.bomb1.alpha = 0.4
    } else if (bombNum == 1) {
      this.bomb2.alpha = 0.4
      this.bomb1.alpha = 1
    } else {
      this.bomb2.alpha = 1
      this.bomb1.alpha = 1
    }
  }

  updatePlayer(player: Player) {
    if(this.playerImages[player.id])
    {
      console.log("Updated player");
      this.animateMove(this.playerImages[player.id], player.pos);
    }
    else
    {
      this.addPlayer(player)
    }
  }

  updateEnemy(enemy: Enemy) {
    if(this.enemyImages[enemy.id]) {
      console.log("Updated enemy");
      this.animateMove(this.enemyImages[enemy.id], enemy.pos);
    } else {
      this.addEnemy(enemy)
    }
  }

  private animateMove(target: Phaser.GameObjects.GameObject, targetPosition: ObjectPosition) {
    this.tweens.add({
      targets: target,
      x: 32 * targetPosition.posX + 16,
      y: 32 * targetPosition.posY + 16,
      duration: 50,
    })
  }

  private addEnemy(enemy: Enemy){
    console.log("Added enemy");

    this.enemyImages[enemy.id] = this.add.sprite(32 * enemy.pos.posX + 16, 32 * enemy.pos.posY + 16, 'characters', 51)
    this.enemyImages[enemy.id].setDisplaySize(32, 32);
  }

  disconnect(playerId: string) {
    this.playerImages[playerId]?.destroy()
    delete this.playerImages[playerId]
  }

  private addPlayer(player: Player){
    console.log("Added player");

    this.playerImages[player.id] = this.add.sprite(32 * player.pos.posX + 16, 32 * player.pos.posY + 16, 'characters', player.character)
    this.playerImages[player.id].setDisplaySize(32, 32);
  }

  powerupSpawn(powerup: Powerup) {
    let powerupImage;
    switch(powerup.type){
      case PowerupType.EXTRA_LIFE:
        powerupImage = "powerup_extra_life"; break;
      case PowerupType.EXTEND_EXPLOSION_TIME:
        powerupImage = "powerup_increase_exploding_time"; break;
      case PowerupType.EXTEND_EXPLODING_RANGE:
        powerupImage = "powerup_increase_explosion_size"; break;
      default:
        throw new Error(`CAN'T FIND IMAGE FOR POWERUP TYPE ${powerup.type}`)
    }
    this.powerupImages[powerup.id] = this.add.image(32 * powerup.posX + 16, 32 * powerup.posY + 16, powerupImage)
    this.powerupImages[powerup.id].setDisplaySize(32, 32)
  }

  powerupDestroy(powerupId: string) {
    this.powerupImages[powerupId]?.destroy()
    delete this.powerupImages[powerupId]
  }

  getLivesText(): string{
    return `Lifes: ${this.gameEngine.player.lifes}`
  }

  updateScore() {
    const playersSorted = [ ...Object.values(this.gameEngine.players)].sort((a, b) => b.score - a.score == 0 ? a.name.localeCompare(b.name) : b.score - a.score);
    let i = 0;
    playersSorted.forEach(p => {
      if (this.scoreLines[i] != undefined) {
        this.scoreLines[i].sprite.setTexture('characters', p.character)
        this.scoreLines[i].text.setText(`${p.name}:${p.score}`);
      } else {
        console.log("adding")

        const sprite = this.add.sprite(10, 35 + i * 25, 'characters', p.character)
        sprite.setDisplaySize(22, 22);
        const text = this.add.text(25, 27 + i * 25, '', { font: '12px Times', fill: '#000000' });
        this.scoreContainer.add(sprite);
        this.scoreContainer.add(text);
        this.scoreLines.push( {sprite, text} );
      }
      i++;
    });

    if ( i < this.scoreLines.length ) {
      console.log("removing")

      for (let j = i; j < this.scoreLines.length; j++){
        this.scoreLines[j].sprite.destroy();
        this.scoreLines[j].text.destroy();
      }
      this.scoreLines.splice(this.scoreLines.length - i);
    }
  }
}