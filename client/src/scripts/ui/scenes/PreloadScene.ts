import Tilemap = Phaser.Tilemaps.Tilemap;
import MapData = Phaser.Tilemaps.MapData;

export default class PreloadScene extends Phaser.Scene {
  constructor() {
    super({ key: 'PreloadScene' })
  }

  preload() {
    this.load.image('tiles', 'assets/tilemaps/tiles/drawtiles-spaced.png');
    this.load.image('car', 'assets/sprites/car90.png');
    this.load.image('bomb', 'assets/sprites/bomb.png');

    this.load.image('powerup_extra_life', 'assets/sprites/powerup_extra_life.png');
    this.load.image('powerup_increase_exploding_time', 'assets/sprites/powerup_increase_exploding_time.png');
    this.load.image('powerup_increase_explosion_size', 'assets/sprites/powerup_increase_explosion_size.png');

    this.load.spritesheet('explosion', 'assets/sprites/explosion.png', { frameWidth: 32, frameHeight: 32});
    this.load.tilemapCSV('map', 'assets/tilemaps/csv/grid.csv');
    this.load.spritesheet('characters', 'assets/sprites/characters.png', { frameWidth: 64, frameHeight: 64 });
  }

  create() {
    this.scene.start('MainScene')
  }
}
